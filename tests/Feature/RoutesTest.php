<?php

test('requests to home page successfully return 200', function () {
    $response = $this->get('/');

    $response->assertStatus(200);
});

test('request to about page successfully return 200', function () {
    $response = $this->get('/about');

    $response->assertStatus(200);
});
