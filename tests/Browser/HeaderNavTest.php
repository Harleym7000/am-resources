<?php

use Laravel\Dusk\Browser;

test('HeaderNav component shows all available nav links', function () {
    $this->browse(function (Browser $browser) {
        $browser->visit('/')
            ->assertSee('Home')
            ->assertSee('About')
            ->assertSee('Services')
            ->assertSee('News')
            ->assertSee('Contact Us');
    });
});

test('HeaderNav nav links navigate to the expected pages', function () {
    $this->browse(function (Browser $browser) {
        $browser->visit('/')
            ->click("@homeLink")
            ->assertPathIs("/")
            ->click("@aboutLink")
            ->assertPathIs("/about")
            ->click("@servicesLink")
            ->assertPathIs("/services")
            ->click("@newsLink")
            ->assertPathIs("/news")
            ->click("@contactLink")
            ->assertPathIs("/contact-us");
    });
});
