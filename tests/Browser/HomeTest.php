<?php

use Laravel\Dusk\Browser;

test('Renders home page', function () {
    $this->browse(function (Browser $browser) {
        $browser->visit('/')
                ->assertSee('Helping NI non-profits profit');
    });
});
