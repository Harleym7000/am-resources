const getActiveTab = () => {
    const location = window.location.pathname;

    switch(location) {
        case "/calendar" :
            return 1;
        case "/workitems" :
            return 2;
        default :
            return 0;
    }
};

export {
    getActiveTab
};
