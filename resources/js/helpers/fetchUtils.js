import "isomorphic-fetch";

let fetchAsyncGet = async url => {
    return await fetch(url, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    });
};

let fetchAsyncPost = async (url, body) => {
    return await fetch(url, {
        method: "POST",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": document.getElementsByName("csrf-token")[0].content
        },
    });
};
export {fetchAsyncGet, fetchAsyncPost};
