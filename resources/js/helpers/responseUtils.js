import {fetchAsyncGet, fetchAsyncPost} from "@/helpers/fetchUtils.js";

let returnGetResponse = async url => {
    return await fetchAsyncGet(url)
        .then(data => {
            return data.json();
        })
        .catch(error => {
            return error;
        });
};

let returnPostResponse = async (url, body) => {
    return await fetchAsyncPost(url, body)
        .then(data => {
            return data.json();
        })
        .catch(error => {
            return error;
        });
};

export {returnGetResponse, returnPostResponse};
