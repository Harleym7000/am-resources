import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout.jsx';
import {Head} from '@inertiajs/react';
import SideNav from "@/Layouts/SideNav.jsx";
import {getActiveTab} from "@/helpers/GetActiveTabHelper.jsx";
import {Col, Row} from "react-bootstrap";

export default function Calendar({auth}) {

    return (
        <>
            <div className="h-screen">
                <AuthenticatedLayout
                    user={auth.user}
                    header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>}
                >
                    <Row>
                        <Col lg={2}>
                            <SideNav/>
                        </Col>
                        <Col lg={10}>
                            WORK ITEMS
                        </Col>
                    </Row>
                </AuthenticatedLayout>
                <Head title="Dashboard"/>
            </div>
        </>
    );
}
