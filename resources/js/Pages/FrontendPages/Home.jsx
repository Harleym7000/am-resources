import {useState} from "react";
import HeaderNav from "@/Layouts/HeaderNav.jsx";
import Footer from "@/Layouts/Footer.jsx";
import "./Home.css";
import HomeJumbotron from "@/Pages/FrontendPages/HomeJumbotron.jsx";
import HomeVision from "@/Pages/FrontendPages/HomeVision.jsx";

export default function Home() {
    return (
        <>
            <HeaderNav/>
            <div className="mainContent">
                <HomeJumbotron/>
                <HomeVision/>
            </div>
            <div className="h-screen">
                TEST 2
            </div>
            <Footer/>
        </>
    );
}
