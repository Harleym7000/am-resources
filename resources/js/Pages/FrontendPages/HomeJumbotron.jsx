import {Button} from "react-bootstrap";
import "./Home.css";

export default function HomeJumbotron() {
    return (
        <>
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                <div className="order-2 md:order-1">
                    <h1 className="font-bold text-yellow-600 text-5xl text-center mt-16 md:text-5xl md:mt-16 xl:text-7xl lg:mt-24 xl:mt-28">AM
                        RESOURCES</h1>
                    <h4 className="text-center">The early bird catches the worm</h4>
                    <h2 className="text-center mt-12 xl:mt-16 md:text-3xl xl:text-4xl">Helping NI non-profits <span
                        className="text-yellow-600 font-bold">profit</span>
                    </h2>
                    <h5 className="text-center text-lg ml-4 mr-4 xl:text-2xl">With over 20 years experience working in Community Outreach
                        consultancy, AM Resources can help your non-profit organisation with funding, training,
                        workshop delivery and much more!
                    </h5>
                    <div className="text-center pb-12 mt-12">
                        <Button variant="warning" className="getStartedButton text-white w-50">Get
                            started!</Button>
                    </div>
                </div>
                <div className="order-1 md:order-2">
                    <img src="/img/am_resources_home.jpeg" alt="AM Resources Consultancy" className="w-full h-full object-cover homeImage"/>
                </div>
            </div>
        </>
    );
}
