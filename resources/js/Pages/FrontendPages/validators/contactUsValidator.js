const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

let validateIsFirstNameValid = (field, firstName) => {
    field.target.className = "form-control";
    if (!firstName) {
        field.target.className = field?.target?.className + " is-invalid";
        return "This field is required";
    }
    if (firstName?.length > 50) {
        field.target.className = field?.target?.className + " is-invalid";
        return "Maximum characters: 50";
    }
    field.target.className = field?.target?.className + " is-valid"
    return " ";
}

let validateIsLastNameValid = (field, lastName) => {
    field.target.className = "form-control";
    if (!lastName) {
        field.target.className = field?.target?.className + " is-invalid";
        return "This field is required";
    }
    if (lastName?.length > 50) {
        field.target.className = field?.target?.className + " is-invalid";
        return "Maximum characters: 50";
    }
    field.target.className = field?.target?.className + " is-valid"
    return " ";
}

let validateIsEmailValid = (field, emailAddress) => {
    field.target.className = "form-control";
    if (!emailAddress) {
        field.target.className = field?.target?.className + " is-invalid";
        return "This field is required";
    }
    if (emailAddress?.length > 150) {
        field.target.className = field?.target?.className + " is-invalid";
        return "Maximum characters: 150";
    }
    const validRegex = emailPattern.test(emailAddress);
    if (!validRegex) {
        field.target.className = field?.target?.className + " is-invalid";
        return "Please provide a valid email address";
    }
    field.target.className = field?.target?.className + " is-valid"
    return " ";
}

let validateIsSubjectValid = (field, subject) => {
    field.target.className = "form-control";
    if (!subject) {
        field.target.className = field?.target?.className + " is-invalid";
        return "Please select a dropdown value";
    }
    field.target.className = field?.target?.className + " is-valid"
    return " ";
}

let validateIsMessageValid = (field, message) => {
    field.target.className = "form-control";
    if (!message) {
        field.target.className = field?.target?.className + " is-invalid";
        return "This field is required";
    }
    field.target.className = field?.target?.className + " is-valid"
    return " ";
}

export {validateIsFirstNameValid, validateIsLastNameValid, validateIsEmailValid, validateIsSubjectValid, validateIsMessageValid};
