import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head} from '@inertiajs/react';
import {useState} from "react";
import PrimaryButton from "@/Components/PrimaryButton.jsx";
import HeaderNav from "@/Layouts/HeaderNav.jsx";

export default function About() {

    return (
        <>
            <HeaderNav>
            </HeaderNav>
            <h1 className="mt-2">ABOUT PAGE</h1>
            <h3 className="mt-2">I have worked for over 21 years within the Community and Voluntary Sector Previously I
                had worked in IT for 10 years I am passionate about Peace and Reconciliation strategies, Community, Good
                Relations , Community Development, local culture and heritage and creating opportunities to nurture
                shared space and safer communitues. Community Education and creating more opportunities for capacity
                building thus ensuring sustainable communities for the future The empowerment of women, the civic voice
                to be supported and utilised to enhance local communities and address need. I was previously employed by
                Building Communities Resource Centre working on Community Relations issues within Causeway Coast and
                Glens Borough Council. My role involved working within The 'Together Building An United Community
                'framework which looks at creative ways of ensuring Shared
                and safe communitues, looking at cultural expression in all its many guises and supporting our young
                people. I am also an ex Independent Councillor within Causeway Coast and Glens Borough Council. I served
                on the council fior over 5 years focusing on improving local communities for eveyone.</h3>
        </>
    );
}
