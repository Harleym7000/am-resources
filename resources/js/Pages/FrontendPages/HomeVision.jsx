import React from "react";
import {Col, Row, Image} from "react-bootstrap";

export default function HomeVision() {
    return (
        <>
            <div className="bg-yellow-600 h-full pb-40">
                <h1 className="text-center pt-16 text-white text-7xl font-bold">OUR VISION</h1>
                <Row>
                    <Col sm={12} lg={4}>
                        <Image src="/img/amres.png" className="relative left-1/2 transform -translate-x-1/2 w-3/4 "/>
                    </Col>
                    <Col sm={12} lg={8}>
                        <p className="pb-40 md: pb-52 text-white text-2xl lg:text-4xl md:mt-1 lg:mt-24 font-bold ml-3 mr-3">AM Resources is passionate about sustaining communities and creating a stronger community and
                            voluntary sector, providing
                            groups and organisations with the tools to succeed. <br/><br/>AM Resources will help you provide a
                            platform to bring communities and
                            ideas together to positively impact on their lives. At AM Resources, we believe community is
                            common unity, and our aim is
                            uniting communities for positive impact!</p>
                    </Col>
                </Row>
            </div>
        </>
    );
}
