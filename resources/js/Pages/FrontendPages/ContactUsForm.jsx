import React, {useState} from "react";
import {Button, Card, Col, Form, Row, Spinner} from "react-bootstrap";
import {returnPostResponse} from "@/helpers/responseUtils.js";
import {
    validateIsEmailValid,
    validateIsFirstNameValid,
    validateIsLastNameValid,
    validateIsMessageValid, validateIsSubjectValid
} from "./validators/contactUsValidator.js";

export default function ContactUsForm() {

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [message, setMessage] = useState("");
    const [subject, setSubject] = useState("");
    const [emailAddress, setEmailAddress] = useState("");

    const [firstNameError, setFirstNameError] = useState("");
    const [lastNameError, setLastNameError] = useState("");
    const [emailAddressError, setEmailAddressError] = useState("");
    const [subjectError, setSubjectError] = useState("");
    const [messageError, setMessageError] = useState("");

    const [isSubmitting, setIsSubmitting] = useState(false);

    const csrfToken = document.getElementsByName("csrf-token")[0].content

    const handleSubmit = async () => {
        setIsSubmitting(true);
        const response = await returnPostResponse("/contact-us", {firstName, lastName, emailAddress, subject, message});

        switch (response.status) {
            case 200:
                setIsSubmitting(false);
                break;
            default:
                setIsSubmitting(false);
        }
    }

    const handleDisableButton = () => {
        return firstName === "" && !firstNameError ||
            lastName === "" && !lastNameError ||
            emailAddress === "" && !emailAddressError ||
            subject === "" && !subjectError ||
            message === "" && !messageError;
    }

    const handleOnChange = (e) => {
        console.log(e.target.name);
        switch (e.target.name) {
            case "firstName":
                setFirstName(e.target.value);
                setFirstNameError(validateIsFirstNameValid(e, e.target.value));
                break;
            case "lastName":
                setLastName(e.target.value);
                setLastNameError(validateIsLastNameValid(e, e.target.value));
                break;
            case "emailAddress":
                setEmailAddress(e.target.value);
                setEmailAddressError(validateIsEmailValid(e, e.target.value));
                break;
            case "subject":
                setSubject(e.target.value);
                setSubjectError(validateIsSubjectValid(e, e.target.value));
                break;
            case "message":
                setMessage(e.target.value);
                setMessageError(validateIsMessageValid(e, e.target.value));
                break;
        }
    }

    return (
        <>
            <Card className="w-full mt-4">
                <Card.Header>
                    Contact Us
                </Card.Header>
                <Card.Body>
                    <Form>
                        <input type="hidden" name="_token" value={csrfToken}/>
                        <Row>
                            <Col md={6}>
                                <Form.Group
                                    className="mb-3"
                                    controlId="exampleForm.ControlInput1">
                                    <Form.Label>
                                        First Name
                                    </Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter first name..."
                                        required
                                        name="firstName"
                                        onChange={(e) => handleOnChange(e)}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {firstNameError}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Col>
                            <Col md={6}>
                                <Form.Group
                                    className="mb-3"
                                    controlId="exampleForm.ControlInput1">
                                    <Form.Label>
                                        Last Name
                                    </Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter last name..."
                                        required
                                        name="lastName"
                                        onChange={(e) => handleOnChange(e)}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {lastNameError}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <Form.Group
                                    className="mb-3"
                                    controlId="exampleForm.ControlInput1">
                                    <Form.Label>
                                        Email Address
                                    </Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Enter email address..."
                                        required
                                        name="emailAddress"
                                        onChange={(e) => handleOnChange(e)}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {emailAddressError}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Col>
                            <Col md={6}>
                                <Form.Group
                                    className="mb-3"
                                    controlId="exampleForm.ControlInput1">
                                    <Form.Label>
                                        Subject
                                    </Form.Label>
                                    <Form.Select
                                        aria-label="Default select example"
                                        name="subject"
                                        onChange={(e) => handleOnChange(e)}
                                        defaultValue="Select..."
                                    >
                                        <option disabled>Select...</option>
                                        <option value="other">Other</option>
                                    </Form.Select>
                                    <Form.Control.Feedback type="invalid">
                                        {subjectError}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Form.Group
                            className="mb-3 messageText"
                            controlId="exampleForm.ControlTextarea1">
                            <Form.Label>
                                Message
                            </Form.Label>
                            <Form.Control
                                as="textarea"
                                rows={7}
                                name="message"
                                onChange={(e) => handleOnChange(e)}
                            />
                            <Form.Control.Feedback type="invalid">
                                {messageError}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <div className="text-right mt-5">
                            <Button
                                className={handleDisableButton() ? "btn btn-secondary" : "btn btn-primary"}
                                disabled={handleDisableButton()}
                                onClick={(e) => handleSubmit(e)}>
                                {isSubmitting ?
                                    <>
                                        <Spinner
                                            as="span"
                                            animation="border"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                        />
                                        <span className="visually-hidden">Loading...</span>
                                    </> : ""}
                                Submit
                            </Button>
                        </div>
                    </Form>
                </Card.Body>
            </Card>
        </>
    )
}
