import HeaderNav from "@/Layouts/HeaderNav.jsx";
import ContactUsForm from "@/Pages/FrontendPages/ContactUsForm.jsx";
import Container from "react-bootstrap/Container";

export default function Contact() {

    return (
        <>
            <HeaderNav>
            </HeaderNav>
            <Container className="flex h-3/4 w-full items-center">
                <ContactUsForm/>
            </Container>
        </>
    );
}
