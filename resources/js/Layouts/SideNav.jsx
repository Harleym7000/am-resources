import {getActiveTab} from "@/helpers/GetActiveTabHelper.jsx";

export default function SideNav() {
    const activeTab = getActiveTab()

    return (
        <>
            <div className="h-screen bg-white border-r-4 border-gray-300">
                <div className="pt-4"></div>
                <a href="/calendar" className="no-underline">
                    <div
                        className={activeTab === 1 ? "sideNavItem pl-8 h-14 pt-3 pb-2 bg-blue-200 m-2 rounded-xl" : "sideNavItem h-14 pl-8 pt-3 m-2"}>
                        <div className="grid grid-cols-12">
                            <div className="cols-span-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill={activeTab === 1 ? "blue" : "gray"}
                                     className="bi bi-calendar4" viewBox="0 0 16 16">
                                    <path
                                        d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1z"/>
                                </svg>
                            </div>
                            <div className="col-span-9">
                                <span className="text-gray-500 ml-4">Calendar</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="/workitems" className="no-underline">
                    <div
                        className={activeTab === 2 ? "sideNavItem pl-8 h-14 pt-3 pb-2 bg-blue-200 m-2 rounded-xl" : "sideNavItem h-14 pl-8 pt-3 m-2"}>
                        <div className="grid grid-cols-12">
                            <div className="cols-span-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill={activeTab === 2 ? "blue" : "gray"}
                                     className="bi bi-journal-text" viewBox="0 0 16 16">
                                    <path
                                        d="M5 10.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5m0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5"/>
                                    <path
                                        d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2"/>
                                    <path
                                        d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1z"/>
                                </svg>
                            </div>
                            <div className="col-span-9">
                                <span className="text-gray-500 ml-4">Work Items</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="/invoices" className="no-underline">
                    <div
                        className={activeTab === 3 ? "sideNavItem pl-8 h-14 pt-3 pb-2 bg-blue-200 m-2 rounded-xl" : "sideNavItem h-14 pl-8 pt-3 m-2"}>
                        <div className="grid grid-cols-12">
                            <div className="cols-span-3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill={activeTab === 3 ? "blue" : "gray"}
                                     className="bi bi-receipt-cutoff" viewBox="0 0 16 16">
                                    <path
                                        d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5m0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5m0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5m0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5m0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5M11.5 4a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1z"/>
                                    <path
                                        d="M2.354.646a.5.5 0 0 0-.801.13l-.5 1A.5.5 0 0 0 1 2v13H.5a.5.5 0 0 0 0 1h15a.5.5 0 0 0 0-1H15V2a.5.5 0 0 0-.053-.224l-.5-1a.5.5 0 0 0-.8-.13L13 1.293l-.646-.647a.5.5 0 0 0-.708 0L11 1.293l-.646-.647a.5.5 0 0 0-.708 0L9 1.293 8.354.646a.5.5 0 0 0-.708 0L7 1.293 6.354.646a.5.5 0 0 0-.708 0L5 1.293 4.354.646a.5.5 0 0 0-.708 0L3 1.293zm-.217 1.198.51.51a.5.5 0 0 0 .707 0L4 1.707l.646.647a.5.5 0 0 0 .708 0L6 1.707l.646.647a.5.5 0 0 0 .708 0L8 1.707l.646.647a.5.5 0 0 0 .708 0L10 1.707l.646.647a.5.5 0 0 0 .708 0L12 1.707l.646.647a.5.5 0 0 0 .708 0l.509-.51.137.274V15H2V2.118z"/>
                                </svg>
                            </div>
                            <div className="col-span-9">
                                <span className="text-gray-500 ml-4">Invoices</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </>
    )
}
