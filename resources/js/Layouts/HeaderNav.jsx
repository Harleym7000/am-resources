import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import './HeaderNav.css';

function HeaderNav() {
    return (
        <Navbar expand="xl" className="bg-white drop-shadow-xl h-1/4 w-screen">
            <Container className="w-screen">
                <img src="/img/logo.png" alt="AM Resources logo" width="150px"/>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto"></Nav>
                    <Nav className="d-flex justify-content-end">
                        <Nav.Link className="nav-link" href="/" dusk="homeLink">Home</Nav.Link>
                        <Nav.Link className="nav-link" href="/about" dusk="aboutLink">About</Nav.Link>
                        <Nav.Link className="nav-link" href="/services" dusk="servicesLink">Services</Nav.Link>
                        <Nav.Link className="nav-link" href="/contact-us" dusk="contactLink">Contact Us</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default HeaderNav;
