<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use App\Mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function index(ContactUsRequest $request)
    {
        if ($request->validated()) {
            $sendEmail = Mail::send('contact', [
                'body' => $request->get("message"),
                'firstName' => $request->get("firstName"),
                'lastName' => $request->get("lastName")
            ], function ($mail) use ($request) {
                $mail->from($request->get("emailAddress"), $request->get("emailAddress"));
                $mail->to(env('MAIL_FROM_ADDRESS'))->subject($request->get("subject"));
            });
        }
    }
}
