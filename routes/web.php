<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ContactUsController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/calendar', function () {
    return Inertia::render('BackendPages/Calendar');
})->middleware(['auth'])->name('calendar');

Route::get('/workitems', function () {
    return Inertia::render('BackendPages/WorkItems');
})->middleware(['auth'])->name('workitems');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/', function () {
   return Inertia::render('FrontendPages/Home');
})->name('home');

Route::get('/about', function () {
    return Inertia::render('FrontendPages/About');
})->name('about');

Route::get('/services', function () {
    return Inertia::render('FrontendPages/Services');
})->name('services');

Route::get('/news', function () {
    return Inertia::render('FrontendPages/News');
})->name('news');

Route::get('/contact-us', function () {
    return Inertia::render('FrontendPages/Contact');
})->name('contact');

Route::post('/contact-us', [ContactUsController::class, "index"]);

require __DIR__.'/auth.php';
